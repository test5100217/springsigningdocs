package demo.hello.userinfo;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

/*
 * - Servicio al controlador, el controlador, es decir, el controlador se encarga de poner las endpoints consumidas
 * - Service: es quien hace la logica, y las operaciones CRUD hacia la base de datos, es decir, desde el controlador se llaman a estos metodos
 *   para interactuar  con los drivers como bases de datos y otros (factoria de conexiones)
 */
@Service
@RequiredArgsConstructor
public class ApretonService 
{

    /*
     * Inyectamos el repositorio, que es quien tiene acceso a las bases de datos
     * Formas de inyectar, usualmente se realiza en el constructor o utilizando autowired
     */

    // Necesita el constructor para inicializarlo y va a contener metodos basicos CURD para operar en la base de datos
    // Necesitamos inicializarlo en el constructor, por eso @RequiredArgsConstructor
    private final RepositorySaludo repoSal;

    // Podremos tener los metodos basicos
    
    public void crearApreton(UserInfo handshake) 
    {
        repoSal.save(handshake);
         
    }

    public boolean existerUsuario(String firstName)
    {
        return repoSal.existsByFirstName(firstName);
    }

    public UserInfo obtenerUsuario(String firstName)
    {
        UserInfo userInfoEncontrado = repoSal.findByFirstName(firstName);
        return userInfoEncontrado;
    }
    
    
}
