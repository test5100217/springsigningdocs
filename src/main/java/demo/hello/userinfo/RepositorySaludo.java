package demo.hello.userinfo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * RepositorySaludo
 * Esta interfaz es necesaria para crear el DAO, para acceder a la base de datos implementando con JPA
 * 
 */

 /*  @Repository
  *  Indicates that an annotated class is a "Repository", originally defined by Domain-Driven Design (Evans, 2003) as "a mechanism for encapsulating storage, 
  *  retrieval, and search behavior which emulates a collection of objects".
  *
  *  Teams implementing traditional Jakarta EE patterns such as "Data Access Object" may also apply this stereotype to DAO classes, though care should be 
  *  taken to understand the distinction between Data Access Object and DDD-style repositories before doing so. This annotation is a general-purpose 
  *  stereotype and individual teams may narrow their semantics and use as appropriate.
 */


@Repository
public interface RepositorySaludo extends JpaRepository<UserInfo, byte[]> {
    /*
    * Interfaz RepositorySaludo se extiende de JpaRepository<Entidad, Tipo de datos del identificador de la entidad> 
    * Interface JpaRepository<T,ID> (T es la clase de la entidad e ID es el tipo del ID de la entidad):
    *     A JpaRepository is a high-level interface that is a part of the Spring Data JPA framework. 
    *     It provides a set of methods for performing common CRUD (Create, Read, Update, Delete) operations on a database table.
    */
    boolean existsByFirstName(String firstName);
    UserInfo findByFirstName(String firstName);
}