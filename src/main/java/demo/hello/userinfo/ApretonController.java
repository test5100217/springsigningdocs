package demo.hello.userinfo;

import demo.hello.signing.SignedDoc;
import demo.hello.signing.SignedDocService;
import demo.hello.utilities.Tuple;
import jakarta.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.ssl.SslBundleProperties.Key;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.InvalidKeyException;
import java.security.KeyFactory;


import org.slf4j.Logger;
// Exponer endpoint y funcionalidades
// Implementa el protocolo HTTP
@RestController
@RequestMapping("/apreton")
@RequiredArgsConstructor
public class ApretonController 
{

    private final ApretonService apretonService;
    private final SignedDocService signedService;
    private final Logger logger =  LoggerFactory.getLogger(ApretonController.class);


    // AES-GCM
    private Tuple<PublicKey, PrivateKey> genKeyPair() 
    {
        Tuple<PublicKey, PrivateKey> pair;
        KeyPairGenerator keyPair;
        KeyPair kp;
        try {
            keyPair = KeyPairGenerator.getInstance("RSA");
            keyPair.initialize(2048);
            kp = keyPair.generateKeyPair();
            PublicKey pub = kp.getPublic();
            PrivateKey pvt = kp.getPrivate();
            pair = new Tuple<>(pub, pvt);
        } catch (NoSuchAlgorithmException e) {
            pair = null;
        }
        return pair;

    }


    @PostMapping("/create")
    public ResponseEntity<?>  createApreton(@RequestBody UserInfo apreton)
    {
        Tuple<PublicKey, PrivateKey> pair;
        if (
            apreton.getFirstName() != null && 
            apreton.getUserKpb() == null &&
            apreton.getUserKpv() == null )
        {

            // Imprime logs de creacion
            logger.info("Request Name:  " + apreton.getFirstName());
            logger.info("Se ha creado un usuario correctamente");

            // Generar par de claves para firmar documentos
            pair = genKeyPair();

            // Almacenar en el objeto el par de claves
            apreton.setUserKpb(pair.getX().getEncoded());
            apreton.setUserKpv(pair.getY().getEncoded());

            // Informacion del formato del par de claves
            logger.info("Formato de Kpb --> " + pair.getX().getFormat());
            logger.info("Formato de Kpv --> " + pair.getY().getFormat());

            // Informacion de la longitud del par de claves
            logger.info("Formato de Kpb --> " + pair.getX().getEncoded().length);
            logger.info("Formato de Kpv --> " + pair.getY().getEncoded().length);
            
            // Ponemos que el usuario y las keys son validas
            apreton.setValid(true);
            
            // El servicio almacena en la base de datos
            apretonService.crearApreton(apreton);
            return ResponseEntity.created(null).build();
            
        }else
        {
            logger.info("Solicitud erronea");
            return ResponseEntity.badRequest().build();
        }
    }

    private StringBuilder bytesToHex(byte[] chain){
        StringBuilder sb = new StringBuilder();
        for (byte b : chain) {
            sb.append(String.format("%02X ", b));
        }
        return sb;
    }

    // SignDocument
    private byte[] signDocuments(byte[] requestFile, UserInfo userData) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException
    {
        // Generamos la Clave Privada: Bytes ==> KeySpec (Formao) ==> KeyFactory ==> PrivateKey
        /* KeySpec: KeySpec interface are supposed to reveal metadata about the key
         * This metadata is never used actually in common uses of the key(encryption etc.), 
         * but only used if there is need to look into the mathematical attributes of the key. 
         */
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(userData.getUserKpv());
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey kpv = keyFactory.generatePrivate(keySpec);

        // Firmar
        Signature sign = Signature.getInstance("SHA256withRSA");
        sign.initSign(kpv);
        sign.update(requestFile);
        byte[] result = sign.sign();
        return result;
    }

    @PostMapping("/signdoc")
    public ResponseEntity<?> crearSignedDoc(@RequestParam("file") MultipartFile requestFile, 
                                            @RequestParam("firstName") String firstName,
                                            HttpServletRequest request)
    {
        SignedDoc documentToSign;
        UserInfo userDoc;
        try
        {
            documentToSign = new SignedDoc();
            if (!apretonService.existerUsuario(firstName)){
                throw new Exception();
            }

            // Obtener el usuario
            userDoc = apretonService.obtenerUsuario(firstName);

            // Firmamos el documento y asignamos a ForeignKey
            documentToSign.setRequesterUserId(userDoc);

            // Obtenemos el fichero bruto a firmar
            documentToSign.setGrossFile(requestFile.getBytes());
            // Asiganmos la ForeignKey
            documentToSign.setSignedfile(signDocuments(requestFile.getBytes(), userDoc));
            signedService.crearSignedDoc(documentToSign);
            return ResponseEntity.created(new URI(request.getRequestURI() + "/check")).build();
        } catch (Exception e) 
        {
            return ResponseEntity.badRequest().build();
        } 
    }

    private boolean verifySignedDoc(SignedDoc doc) throws Exception
    {
        // Codificamos la Kpb en X509 del usuario
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(doc.getRequesterUserId().getUserKpb());
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey kpb = keyFactory.generatePublic(keySpec);
        // Empezamos la verificacion con initVerify(), para firmar usamos fue initSign()
        Signature sign = Signature.getInstance("SHA256withRSA");
        sign.initVerify(kpb);
        sign.update(doc.getGrossFile());

        boolean checkDoc;
        checkDoc = sign.verify(doc.getSignedfile());
        logger.info("Resultado de verify (2) --> " + checkDoc);
        return checkDoc;
    }

    @GetMapping(value = "/check/{firstName}", produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<?> verifySignedDoc(@PathVariable String firstName){
        logger.info("Texto firmado --> " + firstName);
        UserInfo userDoc = apretonService.obtenerUsuario(firstName);
        SignedDoc doc = signedService.obtenerSignedDoc(userDoc);
        logger.info("Texto firmado --> " + bytesToHex(doc.getSignedfile()).toString());
        try 
        {
            
            Map<String, String> body = new HashMap<>();
            body.put("Kpb", Base64.getEncoder().encodeToString(userDoc.getUserKpb()));
            body.put("SignedFile", Base64.getEncoder().encodeToString(doc.getSignedfile()));
            verifySignedDoc(doc);
            return new ResponseEntity<>(body, HttpStatus.OK);
        } catch (Exception e) 
        {
            return ResponseEntity.badRequest().build();
        }
        
    }


}
