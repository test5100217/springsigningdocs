package demo.hello.userinfo;

import demo.hello.signing.*;
import java.util.List;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.annotation.Generated;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/*
 * Hibernate a crear la tabla dentro de la BBDD, es decir, en MySQL crearemos la Base de datos (el esquema) pero
 * hibernate creara la tabla. Para ello, utilizara un @Repository en la clase
*/
// @Data genera getter y setter
// @Entity indica que esta clase es una entidad
// @AllArgsConstructor generates an all-args constructor. An all-args constructor requires one argument for every field in the class.
// @NoArgsConstructor Generates a no-args constructor. Will generate an error message if such a constructor cannot be written due to the existence of final fields
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class UserInfo {
    // @Id pecifies the primary key of an entity en una BBDD
    /* 
        @GeneratedValue = The GeneratedValue annotation may be applied to a primary key property or field of an entity or mapped superclass in 
        conjunction with the Id annotation. The use of the GeneratedValue annotation is only required to be supported for simple primary keys. 
        Use of the GeneratedValue annotation is not supported for derived primary keys.
    */ 
    @Column(name = "firstName", unique = true)
    private String firstName;

    // @JsonIgnore para ignorar si en el JSON nos envían el userHash, es decir, ignora el valor durante la serializacion
    @Id
    @GeneratedValue(generator = "byte-array-id-generator", strategy = GenerationType.IDENTITY)
    @GenericGenerator(name = "byte-array-id-generator", strategy = "demo.hello.utilities.ByteArrayIdGenerator")
    @Column(columnDefinition = "BINARY(32)")
    private byte[] userId;

    // No se va a serializar
    @JsonIgnore
    @Lob
    @Column(length=1000000)
    private byte[] userKpb;

    @JsonIgnore
    @Lob
    @Column(length=1000000)
    private byte[] userKpv;

    @JsonIgnore
    private boolean valid;

    @OneToMany(fetch =FetchType.LAZY, mappedBy = "requesterUserId")
    private List<SignedDoc> signedDocs;
}
