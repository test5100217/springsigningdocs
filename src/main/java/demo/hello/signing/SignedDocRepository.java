package demo.hello.signing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.hello.userinfo.UserInfo;

@Repository
public interface SignedDocRepository extends JpaRepository<SignedDoc, byte[]> {
    
    SignedDoc findByrequesterUserId(UserInfo userData);
}
