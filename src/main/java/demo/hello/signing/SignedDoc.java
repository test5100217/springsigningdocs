package demo.hello.signing;

import java.io.File;
import java.util.Set;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.mapping.Collection;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;

import demo.hello.userinfo.UserInfo;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

/**
 * SignedDoc
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class SignedDoc {

    @Lob
    @Column(length=1000000)
    private byte[] signedfile;

    @Lob
    @Column(length=1000000)
    private byte[] grossFile;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    @JsonIgnore
    private UserInfo requesterUserId;

    @Id
    @GeneratedValue(generator = "byte-array-id-generator")
    @GenericGenerator(name = "byte-array-id-generator", strategy = "demo.hello.utilities.ByteArrayIdGenerator")
    @Column(columnDefinition = "BINARY(32)")
    private byte[] signedReqId;



}