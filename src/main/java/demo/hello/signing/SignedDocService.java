package demo.hello.signing;

import org.springframework.stereotype.Service;

import demo.hello.userinfo.UserInfo;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SignedDocService {

    private final SignedDocRepository repoSign;

    // Podremos tener los metodos basicos
    
    public void crearSignedDoc(SignedDoc signedDoc) 
    {
        repoSign.save(signedDoc);
         
    }

    public SignedDoc obtenerSignedDoc(UserInfo userData)
    {
        SignedDoc signedDoc = repoSign.findByrequesterUserId(userData);
        return signedDoc;
    }
}
