package demo.hello.utilities;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

public class ByteArrayIdGenerator implements IdentifierGenerator
{

    @Override
    public Serializable generate(SharedSessionContractImplementor arg0, Object arg1) throws HibernateException
    {
        byte[] generatedId = generateUniqueId();
        /*
         * En caso de que ecista el ID, se crea otro
         */
        return generatedId;
    }
    
    private byte[] genHash(String uuid) 
    {
        MessageDigest digest;
        byte [] hashDb;
        try 
        {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(uuid.getBytes(), 0, uuid.length());
            hashDb = digest.digest();
        }catch(NoSuchAlgorithmException e)
        {
            hashDb = null;
        }
        return hashDb;
    }

    private byte[] generateUniqueId()
    {
        return genHash(java.util.UUID.randomUUID().toString());
    }

}
